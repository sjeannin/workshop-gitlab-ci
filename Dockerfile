FROM ruby:2.5-alpine
WORKDIR /app
COPY Gemfile Gemfile.lock /app/
ADD ./ /app/
RUN bundle install
ENV PORT 5000
EXPOSE 5000

CMD bundle exec ruby ./server.rb -p $PORT -o 0.0.0.0

